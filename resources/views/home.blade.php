@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($awards as $award)
            <div class="card mb-2">
                <div class="card-header"><strong>{{$award->name}}</strong></div>
                <div class="card-body text-center">
                    <form action="{{route('vote')}}" method="post" class="form" >
                        @csrf
                        <div class="form-group">
                            <input type="hidden" value="{{$award->id}}" name="award">
                            @foreach($award->nominees as $nominee)
                            <div>
                                <input type="radio" name="nominee" value="{{$nominee->id}}" id="{{$nominee->name}}" required>
                                <label for="{{$nominee->name}}">{{$nominee->name}}</label>
                            </div>
                            @endforeach
                        </div>
                        @if($award->hasVoted)
                        <div class="form-group">
                            <button type="button" class="btn btn-lg btn-default" disabled>Voted</button>
                        </div>
                        @else
                        <div class="form-group">
                            <button type="submit" style="background-color: purple; color: #fff; border: none;" class="btn btn-lg btn-success px-5">Vote</button>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
