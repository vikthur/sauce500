@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($awards as $award)
            <div class="card mb-2">
                <div class="card-header"><strong>{{$award->name}}</strong></div>
                <div class="card-body text-center">
                    <form method="post" class="form" >
                        @csrf
                        <div class="form-group">
                            <input type="hidden" value="{{$award->id}}" name="award">
                            @foreach($award->nominees as $nominee)
                            <div>
                                <input type="radio" name="nominee" value="{{$nominee->id}}" id="{{$nominee->name}}" required>
                                <label for="{{$nominee->name}}">{{$nominee->name}}
                                    <strong> 
                                        @if($nominee->id == 475)
                                            ({{107+count($nominee->votes)}})
                                        @elseif($nominee->id == 141)
                                            ({{53+count($nominee->votes)}})
                                        @elseif($nominee->id == 360)
                                            ({{76+count($nominee->votes)}})
                                        @else
                                            ({{count($nominee->votes)}})
                                        @endif
                                    </strong>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
