<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Awards extends Model
{
    public function nominees()
    {
    	return $this->hasMany('App\Nominees');
    }
}
