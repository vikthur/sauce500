<?php

namespace App\Http\Controllers;

use Auth;
use App\Awards;
use App\Voting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $awards = Awards::with('nominees')->get();

        foreach ($awards as $award) 
        {
            if ($this->hasVoted($award->id)) {
               $award->hasVoted = true;
            }else{
                $award->hasVoted = false;
            }
        }

        return view('home', compact('awards'));
    }

    public function vote(Request $request)
    {
        if ($this->hasVoted($request->award)) 
        {
            return redirect()->back();
        }

        $voting = new Voting();

        $voting->nominee_id = $request->nominee;
        $voting->awards_id = $request->award;
        $voting->user_id = Auth::user()->id;

        $voting->save();
        return redirect()->back();
        $awards = Awards::with('nominees')->get();
        
        foreach ($awards as $award) 
        {
            if ($this->hasVoted($award->id)) {
               $award->hasVoted = true;
            }else{
                $award->hasVoted = false;
            }
        }

        return view('home', compact('awards'));
    }

    public function hasVoted($awardId)
    {
        $check = Voting::where('user_id', Auth::user()->id)->where('awards_id', $awardId)->first();
        if ($check) {
          return true;
        }
        return false;
    }

    public function getVotesResults()
    {
        $awards = Awards::with('nominees.votes')->get();
        // dd($awards);
        return view('result', compact('awards'));
    }
}
