<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'matric' => 'required|string',
            'name' => 'required|string',
        ]);

        $user = User::where('matric_number', $request->matric)->first();

        if($user)
        {
            $names = explode(' ', $user->name);
            $reqNameArr = explode(' ', $request->name);
            $arrInt = array_intersect(array_map('strtolower', $names), array_map('strtolower', $reqNameArr));
            $count = count($arrInt);
            // dd($count);
            if ($count > 0) {

                Auth::loginUsingId($user->id, true);

                return redirect()->route('home');
            }else{
                throw ValidationException::withMessages([
                    "matric" => [trans('auth.failed')],
                ]);
            }
        }else{
            throw ValidationException::withMessages([
                    "matric" => [trans('auth.failed')],
                ]);
        }

       
    }

}
