<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominees extends Model
{
    public function votes()
    {
    	return $this->hasMany('App\Voting', "nominee_id");
    }
}
